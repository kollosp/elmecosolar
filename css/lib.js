//file contains scripts connected with lib.css styles.
window.onload = function(){
	//add eventHandler on active 
	Array.prototype.forEach.call(document.querySelectorAll(".input-box input"), function(elem){
		
		elem.addEventListener("focus", function(){
			elem.nextSibling.nextElementSibling.classList.add("active")
		})

		elem.addEventListener("blur", function(){
			if(elem.value == ""){
				elem.nextSibling.nextElementSibling.classList.remove("active")
			}
		})
	})

	//console.log(document.querySelectorAll("button")).forEach(function(el){alert("asdasd")})

	Array.prototype.forEach.call(document.querySelectorAll("button.confirmation"), function(elem){
		
		elem.addEventListener("click", function(){
			elem.nextSibling.nextElementSibling.classList.add("active")
		})
	})

	//function allow p element to show only part of full text inside it. string '//' is a point where,
	//text is cut 
	Array.prototype.forEach.call(document.querySelectorAll("div.long-text-container"), function(elem){
		//console.log(elem.innerHTML)

		//add button showing full text
		let button = document.createElement("a")
		let fullContent = elem.innerHTML //remember what is inside befor editing
		let isFullTextShown = false //flag
		elem.innerHTML = fullContent.substring(0, fullContent.indexOf("//"))
		
		//init button params
		button.classList.add("is-primary")
		button.innerHTML = "czytaj wiecej..."

		elem.appendChild(button)

		button.addEventListener("click", function(){

			if(!isFullTextShown){
				//show full text
				let text = elem.innerHTML;
				elem.innerHTML = fullContent.replace("//", "")
				elem.appendChild(button)
				button.innerHTML = "czytaj mniej."
			}
			else{
				//show cat text
				elem.innerHTML = fullContent.substring(0, fullContent.indexOf("//"))
				elem.appendChild(button)
				button.innerHTML = "czytaj wiecej..."
			}

			isFullTextShown = !isFullTextShown
		})
	})
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}


function addResizeEvent(func) {
  var old = window.onresize;
  if (typeof window.onresize != 'function') {
    window.onresize = func;
  } else {
    window.onresize = function() {
      if (old) {
        old();
      }
      func();
    }
  }
}