var nav = new Vue({
	el: '#nav',
	data: {
		active: false,
		items: [
			{name: 'Strona główna', icon: 'fa-home', isActive: true},
			{name: 'Katalog', icon: 'fa-home', isActive: false},
			{name: 'Eksper radzi', icon: 'fa-home', isActive: false},
			{name: 'Współpracownicy', icon: 'fa-home', isActive: false},
			{name: 'Kontakt', icon: 'fa-home', isActive: false}

		]
	},
	
	methods: {
    
    toggle: function (event) {
	    	if(this.active) this.active=false;
	    	else this.active=true;
	    }
	}
})

var articles = new Vue({
	el: '#articles',
	data:{
		arts: [
			{title: "Trochę ciekawostek o systemach solarnych", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas"+
			 "lacinia. Sed eu ipsum ut leo dictum dignissim eu et enim. Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus. Nunc sit " +
			 "amet posuere ante. Nam non fringilla magna. Nullam sed iaculis orci, id eleifend lectus. Fusce sit amet scelerisque sapien.", image: "art1.png", fullText: true,
			 advSubtitle: "Oszczędności", advDesc: "Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus."},
			{title: "Liczby i dane", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas" +
			 "lacinia. Sed eu ipsum ut leo dictum dignissim eu et enim. Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus. Nunc sit " +
			 "amet posuere ante. Nam non fringilla magna. Nullam sed iaculis orci, id eleifend lectus. Fusce sit amet scelerisque sapien.", image: "art2.png", fullText: false,
			 advSubtitle: "Oszczędności", advDesc: "Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus."},
			{title: "Zalety systemów solarnych", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas" +
			 "lacinia. Sed eu ipsum ut leo dictum dignissim eu et enim. Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus. Nunc sit " + 
			 "amet posuere ante. Nam non fringilla magna. Nullam sed iaculis orci, id eleifend lectus. Fusce sit amet scelerisque sapien.", image: "art1.png", fullText: true,
			 advSubtitle: "Oszczędności", advDesc: "Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus."}
		]
	},
	methods:{
		catText: function(text){
			if(text.length>250)
				return text.substring(0, 250);
			return text;
		},
		
		toggleArtVisible: function(index){
            this.arts[index].fullText = !this.arts[index].fullText;
        }
	}
})

var ad = new Vue({
	el: '#ad',
	data: {
		products:[
			{image: "inverter1.png", name: "Inverter 10kW", desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas", price: "8450"},
			{image: "panel_mono.jpg", name: "Panel monokrystaliczy", desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas", price: "1200"}
		]
	}
})