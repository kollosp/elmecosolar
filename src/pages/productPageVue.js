

///////////////////////////////////////////
//PAGE MAIN VUE OBJECT
///////////////////////////////////////////
var ad = new Vue({
	el: '#product',
	data: {
		product: {
            image: "nophoto.png"
        }
	},
	methods:{
		price_format: function(price){
			return parseFloat(price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ')
		}
	},

	mounted () {

		let jsonUrl = parseUrlParam();

        let setToggle = function(){
            Array.prototype.forEach.call(document.querySelectorAll(".toggler"), function(elem){    
                elem.addEventListener("click", function(){
                    //get toggled element
                    let toggleElem = document.getElementById(elem.getAttribute("data-toggle"))
                    let toggleIcon = Array.from(elem.children)[0] //only icon is inside toggler
                    console.log(toggleIcon)
                    if(toggleElem.classList.contains("shown")){
                        toggleElem.classList.remove("shown")
                        toggleIcon.classList.remove("active")
                    }else{
                        toggleElem.classList.add("shown")
                        toggleIcon.classList.add("active")
                    }   

                })

            })
        }

        axios({
            method: 'post',
            url: '/catalog/sql/productcontent',
            data: {
                id: jsonUrl.id
            }
        }).
        then(response => {
            document.getElementById("content").innerHTML = response.data
            setToggle()
        })          
        

        axios({
            method: 'post',
            url: '/catalog/sql/product',
            data: {
                id: jsonUrl.id
            }
        }).
        then(response =>{
            if(response.data.length)
                this.product = response.data[0]
        }) 

 	
  	}
})
