

var product_display = new Vue({
	el: '#product_display',
	data: {
		productsQtty: [],
		products:[{
			price: 8745,
			name: "inverter 10kW",
			desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas"+
			 "lacinia. Sed eu ipsum ut leo dictum //dignissim eu et enim. Sed consectetur"
		}],
		display: {
			index: -1,
			id: "---",
			price: "---",
			name: "---",
			desc: "---"
		}
	},
	methods:{
		previous: function(){
			let self = this
			let object = document.getElementById("product")


			object.style.animation = "product-fadeOut-left 1s linear";
			
			setTimeout(function(){
				let newIndex = self.display.index-1  >= 0 ? self.display.index-1 : self.products.length-1
				self.display = self.products[newIndex]
				self.display.index = newIndex

				object.style.animation = "product-fadeIn-left 1s linear";	
			}, 1000)

		},
		//load next product from catalog
		next: function(){
			let self = this
			let index, rand 

			//hide actual element
			let object = document.getElementById("product")
			object.style.animation = "product-fadeOut-right 1s linear";

			//if exist no loaded product
			if(this.productsQtty.length != 0){
				index = parseInt(Math.random()*1000)%this.productsQtty.length
				rand = this.productsQtty[index]
				this.productsQtty.splice(index, 1)
				
				axios({
					method: 'post',
					url: 'catalog/sql/product',
					data: {
						id: rand 
					}
				}).
				then(response =>{
					console.log(response.data[0])

					if(response.data[0].image)
						if(response.data[0].image == "")
							response.data[0].image = "nophoto.png"

					self.products.push(response.data[0])
					self.display = response.data[0]
					self.display.index = self.products.length-1

					object.style.animation = "product-fadeIn-right 1s linear";	
					
				})
			}
			else{

				setTimeout(function(){
					let index = parseInt(Math.random()*1000)%this.products.length
					this.display = this.products[index]
					self.display.index = index
		
					object.style.animation = "product-fadeIn-right 1s linear"	
				}, 1000)
			}

			setTimeout(function(){
			}, 1000)
		},
		loadNewProduct: function(){
			
		}
	},
	computed: {
		product: function(){
			return this.products[this.products.length-1]
		}
	},
	created: function(){
		let self = this

		let productSwitch = function(){
			
			self.next();
			setTimeout(productSwitch, 10000);
		}


		//read products quantity
		axios({
			method: 'post',
			url: 'catalog/sql/productsQtty'
		}).
		then(response =>{
			for(let i = 0; i<response.data[0].id; ++i)
				self.productsQtty.push(i) 
			
			productSwitch();
		})
	},	
})

var articles = new Vue({
	el: '#articles',
	data:{
		arts: [
			{title: "Trochę ciekawostek o systemach solarnych", content: "1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas"+
			 "lacinia. Sed eu ipsum ut leo dictum //dignissim eu et enim. Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus. Nunc sit " +
			 "amet posuere ante. Nam non fringilla magna. Nullam sed iaculis orci, id eleifend lectus. Fusce sit amet scelerisque sapien.", image: "art1.png", fullText: true,
			 advSubtitle: "Oszczędności", advDesc: "Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus."},
			{title: "Liczby i dane", content: "2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas" +
			 "lacinia. Sed eu ipsum ut leo dictum dignissim eu et enim. Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus. Nunc sit " +
			 "amet posuere ante. Nam non fringilla magna. Nullam sed iaculis orci, id eleifend lectus. Fusce sit amet scelerisque sapien.", image: "art2.png", fullText: false,
			 advSubtitle: "Oszczędności", advDesc: "Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus."},
			{title: "Zalety systemów solarnych", content: "3. Lorem ipsum //dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas" +
			 "lacinia. Sed eu ipsum ut leo dictum dignissim eu et enim. Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus. Nunc sit " + 
			 "amet posuere ante. Nam non fringilla magna. Nullam sed iaculis orci, id eleifend lectus. Fusce sit amet scelerisque sapien.", image: "art1.png", fullText: true,
			 advSubtitle: "Oszczędności", advDesc: "Sed consectetur libero et purus lacinia interdum. Proin bibendum tempus finibus."}
		]
	},
	methods:{
		catText: function(text, isToCat){
			
			let i = text.indexOf("//")
			console.log(text[0], i)
			
			if(i>0 && isToCat)
				return text.substring(0, i)

			return text.replace("//", "")
		},
		
		toggleArtVisible: function(index){
            this.arts[index].fullText = !this.arts[index].fullText;
        }
	}
})



var offer = new Vue({
	el: '#offer'
})

var howToStart = new Vue({
	el: '#howToStart',
	data:{
		tiles:[
			{"nb":"I", "title": "Pierwszy kontakt i ustalenie potrzeb", "icon": "fa-phone", "lis":["zadzwoń do nas napisz maila", "lub wyslij sms", "Dane kontaktowe z zakladce kontakt"]},
			{"nb":"II", "title": "Kalkulacja wstępna", "icon": "fa-file-invoice-dollar", "lis":[]},
			{"nb":"III", "title": "Wizja lokalna albo udostępnienie danych obiektu (lokalizacja, zdjecia)", "icon": "fa-calculator", "lis":["dostacz nam swój rachunek za prad",
			"okresl zapotrzebowanie na energie i sposob jej wykorzystania (np: dzien, noc)", "przygotu zdjecia obiektu lub dokumentacje", "zaproponuj sposób finansowania"]},
			{"nb":"IV", "title": "Oferta", "icon": "fa-file-alt", "lis":[]},
			{"nb":"V", "title": "Zawarcie umowy", "icon": "fa-file-signature", "lis":["na tym etapie w zaleznosci od wartości umowy", "możemy poprosić Cię o wpłate zaliczki"]},
			{"nb":"VI", "title": "Wykonanie projektu", "icon": "fa-pencil-ruler", "lis":["wystarczy, ze wpuścisz nas na kilka chwil do Ciebię, abyśmy mogli wykonać pomiary i uzyskać niezbędne informacje"]},
			{"nb":"VII", "title": "Dostawa urządzeń", "icon": "fa-truck", "lis":["płatność większej części wartości inwestycji"]},
			{"nb":"VIII", "title": "Wykonanie instalacji na obiekcie", "icon": "fa-wrench", "lis":["jesli się tak umówimy, możesz to zrobić sam, a my udzielimy Ci niezbędnych rad"]},
			{"nb":"IX", "title": "Uruchomienie instalacji", "icon": "fa-plane-departure", "lis":[]},
			{"nb":"X", "title": "Uruchomienie zdalnego systemu monitoringu", "icon": "fa-video", "lis":["mozesz wybrać czy chcesz mnieć pełny internetowy wgląd w działanie instalacji (zobacz podgląd)"]},
			{"nb":"XI", "title": "Wniosek do Energetyki i uzgodnienia", "icon": "fa-file-contract", "lis":["uzgodnienia z dostawcą energii możesz zrobić sam, lub powierzyć to nam"]},
			{"nb":"XII", "title": "Odbiór instalacji przez zakład energetyczny", "icon": "fa-handshake", "lis":["przygotuj kalkulator i zacznij liczyć zyski"]},
		]
	},
	mounted: function(){
		//how to start tiles positioning
		let resize = function(){
			const container = document.getElementById("howToStart")
			
			if(window.innerWidth<= 1080) {

				container.style.height = 'auto'
				return
			}
			
			const tiles = document.getElementsByClassName("mytile-parent")
	

			let containerHeight = 0

			for(let i=0;i<tiles.length;++i){
				tiles[i].style.left = ((container.offsetWidth-tiles[0].offsetWidth)/tiles.length)*i
				tiles[i].style.top = i*(tiles[0].offsetHeight+5)
				containerHeight += tiles[0].offsetHeight+5
				console.log(i)
			}

			container.style.height = containerHeight
		}

		resize()
		addResizeEvent(resize)
		//window.onresize = 
	}
})

addLoadEvent(function(){
	
	//why we stairs
	let marginGen = function(){

		let lis = document.getElementsByClassName("whyweli")

		if(window.innerWidth<= 1080) {
			for(let i=0;i<lis.length;++i){
				
			}
		}


		for(let i=0;i<lis.length;++i){
			lis[i].style.marginLeft = (i%4)*40
		}
	}

	addResizeEvent(marginGen())

})