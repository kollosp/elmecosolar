

let prices = new Vue({
	el: '#prices',

	data:{
		subcategories:[{},{}],
		subcategory: "",
		products: [],

		dictionary_plural:{
			"inverter 1 phase": "Invertery jednofazowe",
			"inverter 3 phase": "Invertery trójfazowe",
			"panel monocrystalline": "Panele monokrystaliczne",
			"panel policrystalline": "Panele polikrystaliczne",
			"other accessories": "Akcesoria"
		},
		dictionary:{
			"inverter 1 phase": "Inverter jednofazowy",
			"inverter 3 phase": "Inverter trójfazowy",
			"panel monocrystalline": "Panel monokrystaliczny",
			"panel policrystalline": "Panel polikrystaliczny",
			"other accessories": "Akcesoria"
		}
	},

	mounted: function(){
		//parse url 
		let jsonUrl = parseUrlParam();

		//if url empty et title as all
		if(jsonUrl["subcategory"] == undefined ||
		   jsonUrl["subcategory"] == ""){
			this.subcategory = "Wszyskie produkty"
		}

		axios({
			method: 'post',
			url: 'catalog/sql/subcategories'
		}).
		then(response =>{
			this.subcategories = response.data

			//translate into polish
			for(d in this.subcategories){
				this.subcategories[d]["name"] = this.dictionary_plural[this.subcategories[d]["name"]] ? this.dictionary_plural[this.subcategories[d]["name"]] : this.subcategories[d]["name"]
			}
		})

		//load products list
    	axios({
            method: 'post',
            url: '/catalog/sql/prices',
            data: {
           		//subcategory is an index of subcategory table row. see in db.
              	//in order to dispalay only one category type define subcategory param in url. all categories will be display if nothing in url.   
              	"subcategory": jsonUrl["subcategory"] == undefined ? "all" : jsonUrl["subcategory"] 
            }
        })
      	.then(response => {
      		this.products = response.data

	   		//translate category field into polish
	   		for(p in this.products){
	   			this.products[p].category = this.dictionary[this.products[p].category] ? this.dictionary[this.products[p].category] : this.products[p].category 
	   		}
   		})
	},
	methods: {
		changelocation: function(id){
			window.location.assign("/catalog/product?id="+id)
		}
	}
})