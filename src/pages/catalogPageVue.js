


///////////////////////////////////////////
//PAGE MAIN VUE OBJECT
///////////////////////////////////////////
var ad = new Vue({
	el: '#catalog',
	data: {
		subcategories:[
			{id: 1, name: "kat1"},
			{id: 2, name: "kat2"}
		],
		subcategory: "Nie znaleziono wyników",
		products:[
			{image: "inverter1.png", name: "Inverter 10kW", short_desc: "Lorem ipsum dolor sit amet, consectetur adipismassa egestas", price_netto: "8450"},
			{image: "panel_mono.jpg", name: "Panel monokrystaliczy", short_desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id leo ut massa egestas", price_netto: "1200"}
		],
		dictionary:{
			"inverter 1 phase": "Invertery jednofazowe",
			"inverter 3 phase": "Invertery trójfazowe",
			"panel monocrystalline": "Panele monokrystaliczne",
			"panel policrystalline": "Panele polikrystaliczne",
			"other accessories": "Akcesoria"
		}
	},
	methods:{
		//format price as: X XXXX,XX, after comma goes 'grosze'
		price_format: function(price){
			return parseFloat(price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ')
		},
		//id - id of subcategory 
		reloadData: function(id){
			
			axios({
	              method: 'post',
	              url: '/catalog/sql/products',
	              data: {
	              	//subcategory is an index of subcategory table row. see in db.
	              	//in order to dispalay only one category type define subcategory param in url. all categories will be display if nothing in url.   
	              	"subcategory": id == undefined ? "all" : id 
	              }
	            })
	      	.then(response => {
	      		this.products = response.data
	      			
	      		//if url not empty then use first product category name as title
	      		if(id != undefined){
	      			console.log(this.products)
	      			if(this.products.length > 0){
			      		this.subcategory = this.products[0].cat_name+" "+this.products[0].sub_name
			      		this.subcategory = this.dictionary[this.subcategory] ? this.dictionary[this.subcategory] : "Inne" 
		      		}
		      		else
		      			this.subcategory = "Brak produktów"
		      	}

		      	//remove active class from all buttons 
		      	Array.prototype.forEach.call(document.querySelectorAll("a.subcategory"), function(elem){
					elem.classList.remove("active")
				})

		      	//add active class to pressed button
		      	document.getElementById('subcategory_'+id).classList.add("active")
	      	})
    		
		}
	},
	mounted () {

		//parse url 
		let jsonUrl = parseUrlParam();

		//if url empty et title as all
		if(jsonUrl["subcategory"] == undefined ||
		   jsonUrl["subcategory"] == ""){
			this.subcategory = "Wszyskie produkty"
		}

		//load categories list
		axios({
			method: 'post',
			url: 'catalog/sql/subcategories'
		}).
		then(response =>{
			this.subcategories = response.data
			for(d in this.subcategories){
				this.subcategories[d]["name"] = this.dictionary[this.subcategories[d]["name"]] ? this.dictionary[this.subcategories[d]["name"]] : this.subcategories[d]["name"]
			}
		})

		//load products list
    	axios({
              method: 'post',
              url: '/catalog/sql/products',
              data: {
              	//subcategory is an index of subcategory table row. see in db.
              	//in order to dispalay only one category type define subcategory param in url. all categories will be display if nothing in url.   
              	"subcategory": jsonUrl["subcategory"] == undefined ? "all" : jsonUrl["subcategory"] 
              }
            })
      		.then(response => {
      			this.products = response.data
      			
      			//if url not empty then use first product category name as title
      			if(jsonUrl["subcategory"] != undefined){
	      			this.subcategory = this.products[0].cat_name+" "+this.products[0].sub_name
	      			this.subcategory = this.dictionary[this.subcategory] ? this.dictionary[this.subcategory] : "Inne" 
	      		}
      		})
    	
    	Array.prototype.forEach.call(document.querySelectorAll(".toggle-content"), function(elem){
		
		elem.addEventListener("click", function(){
			//get toggled element
			let toggleElem = document.getElementById(elem.getAttribute("data-toggle"))
			if(toggleElem.style.display == 'none'){
				toggleElem.style.display = 'block'
			}else{
				toggleElem.style.display = 'none'
			}	

		})

	})
  	}
})