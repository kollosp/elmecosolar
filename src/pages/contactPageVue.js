let mailer = new Vue({
	el: "#mailer",
	data: {

	},
	methods: {
		send: function(){
			let allDataCorrect = true

			const email = document.querySelector("#email")
			const name = document.querySelector("#name")
			const title = document.querySelector("#title")
			const surname = document.querySelector("#surname")
			const content = document.querySelector("#content")
			const sendMeBack = document.querySelector("#sendMeBack")

			if(email.value.indexOf("@") == -1){
				email.classList.add("incorrect")
				allDataCorrect = false
			}
			else 
				email.classList.remove("incorrect")

			if(email.value == ""){
				email.classList.add("incorrect")
				allDataCorrect = false
			}else email.classList.remove("incorrect")

			if(name.value == ""){
				name.classList.add("incorrect")
				allDataCorrect = false
			}else name.classList.remove("incorrect")

			if(title.value == ""){
				title.classList.add("incorrect")
				allDataCorrect = false
			}else title.classList.remove("incorrect")

			if(surname.value == ""){
				surname.classList.add("incorrect")
				allDataCorrect = false
			}else surname.classList.remove("incorrect")

			if(content.value == ""){
				content.classList.add("incorrect")
				allDataCorrect = false
			}else content.classList.remove("incorrect")

			if(!allDataCorrect) return 

			const sendButton = document.querySelector("#send")
			const loader = document.createElement("div")
			loader.classList.add("sendWait")
			
			//run send animation
			sendButton.innerHTML = ""
			sendButton.appendChild(loader)

			axios({
				method: 'post',
				url: 'emailSend',
				data: {
					"email": email.value,
					"name": name.value,
					"title": title.value,
					"surname": surname.value,
					"content": content.value,
					"sendMeBack": sendMeBack.checked,
					"send": false,
					"dbsave": false
				}
			}).
			then(response =>{
				console.log(response.data)

				setTimeout(function(){
					//change button state 
					sendButton.innerHTML = "Wysłano"
					sendButton.disabled = true;
					sendButton.classList.add("done")
				}, 1000)
			})
		}
	}
})