const express = require('express')
const mysqlconnect = require('./sqlConnect')
const favicon = require('express-favicon');

const app = express()

//app.use(express.static(__dirname))
app.use('/partials', express.static( __dirname+"/partials"))
app.use(favicon(__dirname+"/../resources/favico/favicon-96x96.png"))
app.use('/root', express.static(__dirname))
app.use(express.static(__dirname+"/../node_modules/bulma/css"))
app.use(express.static(__dirname+"/.."))
app.use(express.static(__dirname+"/../css"))
app.use(express.static(__dirname+"/../resources"))

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(bodyParser.urlencoded({ extended: true }))

app.set('view engine', 'ejs')

const catalog = require("./routes/catalog.js")

app.use('/catalog', catalog)
app.get('/', (req, res) => res.render(__dirname + "/pages/mainPage") )
app.get('/live', (req, res) => res.render(__dirname + "/pages/liveMonitorPage") )
app.get('/expert', (req, res) => res.render(__dirname + "/pages/expertPage") )
app.get('/contact', (req, res) => res.render(__dirname + "/pages/contactPage") )
app.get('/collaborators', (req, res) => res.render(__dirname + "/pages/collaboratorsPage") )


app.get('/prices', (req, res) => {
	let con = mysqlconnect.connect();
    
    con.query("select * from products where enable=1", function (err, result) {
        try{
            if (err) throw err;
            
            res.render(__dirname + "/pages/pricesPage", {"products": result,
														 "dictionary": {"inverter": "inverter", "panel": "panel", "other": "inne"}
														 }) 
        }catch (e){
        
        	console.log(e)
        	res.render(__dirname + "/pages/pricesPage", {"products":[]}) 
        }
    });
})

app.get('/sql', (req, res) => {

	let con = mysqlconnect.connect();

    let data = [];

    if(typeof req.query.query == "undefined"){
        console.log("querry not defind");
        res.send("zle zapytanie");
        return;
  	}

    con.query(req.query.query, function (err, result) {
        try{
            if (err) throw err;
            console.log("wszysko ok");
            console.log(result);
            res.send("wszysko ok "+ result);
      
        }catch (e){
            console.log("Blad zapytania");
            console.log(result);
            res.send("blad "+ result);
        }

  });

})

app.post('/emailSend', function(req, res){
	console.log(req.body)
	let con = mysqlconnect.connect();
	
	if(req.body.dbsave){

		//..saving to db
		con.query("insert into emails value(?,?,?,?,?,?)", [req.body.name, req.body.surname, req.body.email, req.body.content,
			req.body.title, req.body.sendMeBack == false ? "0" : "1"], function (err, result) {
	        try{
	            if (err) throw err;
	            console.log("wszysko ok");
	            res.send("200");
	            console.log(result);
	      
	        }catch (e){
	            console.log("Blad zapytania");
	            console.log(result);
	            res.send("blad "+ result);
	        }

	    });
    }

    if(req.body.send){
    	//.. sending mail
    }

    if(req.body.dbsave == false && req.body.send == false){
    	res.send(200)
    }
	
})


app.get('/download', (req, res) => {


	let con = mysqlconnect.connect();
    con.query("select * from download", function (err, result) {
        try{
            if (err) throw err;
            console.log("wszysko ok");
            console.log(result);
			res.render(__dirname + "/pages/downloadPage", {data: result}) 
      
        }catch (e){
            console.log("Blad zapytania");
            console.log(result);
            res.render("download", {});
        }
    })

})

app.get("/download/:file", function(req, res){
	

	//send file from local storage
    if(req.params.file.indexOf(".pdf")>-1){
		res.setHeader('Content-disposition', 'inline; filename="' + req.params.file + '"');
	 	res.setHeader('Content-type', 'application/pdf');
		res.send(fs.readFileSync(__dirname + "/resources/download/"+req.params.file));
    }else{
    	res.setHeader('Content-disposition', 'attachment; filename='+req.params.file);
    	res.setHeader('Content-type', 'text/plain');
		res.send(fs.readFileSync(__dirname + "/resources/download/"+req.params.file));
    }
 
 	//przekierowanie na strone z ktorej przyszedl rozkaz pobrania pliku
	//wyswietl poprednia strone. jezeli uzytkownik nie wyswietlil jeszcze nic to uruchom powitalna
	//m_res.redirect(m_req.session.lastURL || '/');
});


app.listen(3000, () => console.log('elmeco solar is listening on port 3000'))


process.on('uncaughtException', function(err) {
  	console.log('Caught exception: ' + err);
});