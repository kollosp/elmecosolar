
//vue on scroll animation. before animation state describe in before-enter class,
//animation describe in entry class.
//Usage:
//<div v-animation:className>, before its became visible on screen div will have class before-className. After
//it bacame visible class will be change into className
Vue.directive('animation', {
  inViewport (el) {
    var rect = el.getBoundingClientRect()
    return !(rect.bottom < 0 || /*rect.right < 0 || 
             rect.left > window.innerWidth ||*/
             rect.top > window.innerHeight)
  },

  bind(el, binding) {
    
    if(binding.arg == "" || binding.arg == undefined){
      console.warn("v-animation directive should have argument after, v-animation:name_of_animation_class")
    }

    el.classList.add('before-'+binding.arg)
    
    el.$onScroll = function() {
      if (binding.def.inViewport(el)) {
        el.classList.add(binding.arg)
        el.classList.remove('before-'+binding.arg)
        binding.def.unbind(el, binding)        
      }
    }

    document.addEventListener('scroll', el.$onScroll)
  },
  
  inserted(el, binding) {
    el.$onScroll()    
  },

  unbind(el, binding) {    
    document.removeEventListener('scroll', el.$onScroll)
    delete el.$onScroll
  }  
})

//This function transform url params into json structure
const parseUrlParam = function(){
  let params = window.location.search
    
  if(params[0] == '?')
    params = params.substring(1)

  let array = params.split('&');

  json = {}
    
  for(a in array){
    array[a] = array[a].split('=')
    json[array[a][0]] = array[a][1]
  }

  return json 
}