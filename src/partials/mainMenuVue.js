var nav = new Vue({
	el: '#nav',
	data: {
		active: false,
		items_data: [
			{name: 'Strona główna', icon: 'fa-home', isActive: false, href:"/"},
			{name: 'Katalog', icon: 'fa-home', isActive: false, href:"/catalog"},
			{name: 'Cennik', icon: 'fa-home', isActive: false, href:"/prices"},
			{name: 'Eksper radzi', icon: 'fa-home', isActive: false, href:"/expert"},
			{name: 'Współpracownicy', icon: 'fa-home', isActive: false, href:"/collaborators"},
			{name: 'Kontakt', icon: 'fa-home', isActive: false, href:"/contact"}
		],
		sliders: [ "/Slider0.png", "/Slider1.png", "/Slider2.png", "/Slider3.png"],
		slider: 0
	},
	
	methods: {
    
    toggle: function (event) {
	    	if(this.active) this.active=false;
	    	else this.active=true;
	    }
	},
	computed:{
		items: function(){
			let indexOfActive=-1;
			for(i in this.items_data){
				if(window.location.pathname.indexOf(this.items_data[i].href) != -1) 
					indexOfActive = i
			}

			if(indexOfActive >= 0)
				this.items_data[indexOfActive].isActive = true
			
			console.log(window.location.pathname)
			return this.items_data
		}
	},
	
	created: function() {
		console.log("created")
    	
    	let self = this;
    	this.timer = setInterval(()=>{

		console.log("reload")
    		self.slider++

    		if(self.slider>=self.sliders.length) 
    			self.slider = 0


    	}, 10000)

	}

})