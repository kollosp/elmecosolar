var express = require('express')
var mysqlconnect = require(__dirname + "/../sqlConnect")
var router = express.Router()


router.get("/", function(req, res) {
	
	res.render(__dirname + "/../pages/catalogPage")
})

router.get("/product", function(req, res) {
	
	res.render(__dirname + "/../pages/productPage")
})

//ask for category options
router.post("/sql/subcategories", (req, res)=>{

	let con = mysqlconnect.connect();
	con.query("select s.id, CONCAT(c.name, ' ', s.name) as name from subcategories as s, categories as c where c.id=s.category_id", function (err, result) {
        try{
            if (err) throw err;
      		console.log(result);

      		res.send(JSON.stringify(result))
        }catch (e){
        
        	console.log(e)
      		res.send(e)
        }id
    })

})

//ask for data to prices table
router.post("/sql/prices", (req, res)=>{

	console.log(req.body.subcategory)

	let query = ""
	if(req.body.subcategory == "all") query = "select p.id, p.name, CONCAT(c.name, ' ', s.name) as category, p.price_netto, p.short_desc, p.weight, p.size, p.type	 from products as p, subcategories as s, categories as c where p.subcategory=s.id and c.id=s.category_id and p.enable=1";
	else query = "select p.id, p.name, CONCAT(c.name, ' ', s.name) as category, p.price_netto, p.short_desc, p.weight, p.size, p.type from products as p, subcategories as s, categories as c where p.subcategory=s.id and c.id=s.category_id and p.subcategory=? and p.enable=1";


	console.log("%s: %s", "query", query)

	let con = mysqlconnect.connect();
	con.query(query,[req.body.subcategory], function (err, result) {
        
        try{
            if (err) throw err;
      		console.log(result);
      		res.send(JSON.stringify(result))

        }catch (e){  
        	console.log(e)
      		res.send(e)
        }
    })

})

//ask for all products from category 'category'
router.post("/sql/products/:category", (req, res)=>{

	let con = mysqlconnect.connect();
	con.query("select id, name, type, short_desc, price_netto, image from products" + 
	 "where category=? and enable=1",[req.params.category], function (err, result) {
        
        try{
            if (err) throw err;
      		console.log(result);
      		res.send(JSON.stringify(result))

        }catch (e){  
        	console.log(e)
      		res.send(e)
        }
    })
})

//ask for all data about poduct id='id'
router.post("/sql/products", (req, res)=>{

	//console.log(req.body.subcategory)

	let query = ""
	if(req.body.subcategory == "all") query = "select p.id, p.name, p.image, c.name as cat_name, s.name as sub_name, p.price_netto, p.short_desc from products as p, subcategories as s, categories as c where p.subcategory=s.id and c.id=s.category_id and p.enable=1";
	else  query = "select p.id, p.name, p.image, c.name as cat_name, s.name as sub_name, p.price_netto, p.short_desc from products as p, subcategories as s, categories as c where p.subcategory=s.id and c.id=s.category_id and p.subcategory=? and p.enable=1";


	console.log("%s: %s", "query", query)

	let con = mysqlconnect.connect();
	con.query(query,[req.body.subcategory], function (err, result) {
        
        try{
            if (err) throw err;
      		console.log(result);
      		res.send(JSON.stringify(result))

        }catch (e){  
        	console.log(e)
      		res.send(e)
        }
    })
})

router.post("/sql/productsQtty", (req, res) => {
	let con = mysqlconnect.connect();
	
	con.query("SELECT id FROM products ORDER BY id DESC LIMIT 1", function (err, result) {
        
        try{
            if (err) throw err;
      		console.log(result);
      		res.send(JSON.stringify(result))

        }catch (e){  
        	console.log(e)
      		res.send(e)
        }
    })
})

router.post("/sql/productcontent", (req, res) => {
	let con = mysqlconnect.connect();

	con.query("SELECT html_partial FROM products where id=? limit 1", [req.body.id], function (err, result) {
        
        try{
            if (err) throw err;
      		console.log(result);
      		res.render(__dirname + "/../products/"+result[0].html_partial)

        }catch (e){  
        	console.log(e)
      		res.send(e)
        }
    })
})

router.post("/sql/product", (req, res) => {
	let con = mysqlconnect.connect();
	
	con.query("SELECT id, name, short_desc, price_netto, image FROM products where id=? and enable=1", [req.body.id], function (err, result) {
        
        try{
            if (err) throw err;
      		console.log(result);
      		res.send(JSON.stringify(result))

        }catch (e){  
        	console.log(e)
      		res.send(e)
        }
    })
})



module.exports = router